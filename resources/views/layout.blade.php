<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<link href='https://fonts.googleapis.com/css?family=Merienda' rel='stylesheet'>
		<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

		<title>SmsCRM|@yield('title')</title>
    <link rel="icon" href="images/logo.png">
	</head>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">
	  	<div id="Welcome">
	  		<!---Start navigation Bar -->
	    	<nav class="navbar navbar-expand-lg navbar fixed-top  navbar-light bg-light">
		 		<!--a class="navbar-brand" href="#Welcome">
    				<img src="images/logo.png" width="50" height="50" class="d-inline-block" alt=""> Italian Restaurant
		 		</a-->
		 		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
		    		<span class="navbar-toggler-icon"></span>
		 		</button>
			  	<div class="collapse navbar-collapse" id="navbarText">
			    	<ul class="navbar-nav ml-auto">
			      		
					   
					   
					   @if(Auth::check())
					   <li class="nav-item">
			        		<a class="nav-link" href="/">Welcome&nbsp;{{Auth::user()->name}}</a>
			      		</li>
			      		
					    <li class="nav-item">
					    	<a class="nav-link" href="/user/dashboard/{{Auth::User()->id}}">User Dashboard</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="#Packages">Packages</a>
					    </li>
					   
					   <li class="nav-item">
					    	<a class="nav-link" href="/logout">Logout</a>
					    </li>
					    @else
					     <li class="nav-item">
			        		<a class="nav-link" href="/">Welcome</a>
			      		</li>
					     <li class="nav-item">
					    	<a class="nav-link" href="{{url('/#About')}}">Why Us</a>
					    </li>
					     <li class="nav-item">
					    	<a class="nav-link" href="#Packages">Packages</a>
					    </li>
					     <li class="nav-item">
					    	<a class="nav-link" href="" data-toggle="modal" data-target="#myregister">Register</a>
					    </li>
					   <li class="nav-item">
					    	<a class="nav-link" href="" data-toggle="modal" data-target="#mylogin">Login</a>
					    </li>
					    @endif
				    </ul>
				</div>
			</nav>
			<!--- End Navigation Bar -->
			
			<div class="row" >
				<div class="col-lg-12 text-center" id="sms">	
						@if(session()->has('success2'))
								<div class="alert alert-success alert-dismissible" >
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong> {{ session()->get('success2') }}</strong> 
								</div>
						@endif

						@if(Session::has('error'))
                            <div class="alert alert-sm alert-danger" >
                                 {{ Session::get('error') }}
                                 @php
                                 Session::forget('error');
                                 @endphp
                            </div>
                        @endif


					</div>
			 </div>
			@yield('content')

							
			<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        
          <h4 class="modal-title">Send Message</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>	<form id="sms_form" action="/sms" method="POST">
        	{{ csrf_field() }}
        <div class="modal-body">
        		<div class="form-group col-6">
						  		<label for="inputCel">TO</label>
						  		<input type="tel" class="form-control" id="inputCel" placeholder="Phone" name="phone" required="">
						  	
						  	</div>
         <textarea class="form-control" name="message"  placeholder="Type Message" required></textarea>
        </div>
        <div class="modal-footer">
        	<button type="submit" class="btn btn-info" >Send</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div></form>
      </div>
    </div>
  </div>
<div class="modal fade" id="myregister" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        	
	     		 	
				   <h2 class="modal-title">~ Register ~</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
			@if(session()->has('success'))
   <div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> {{ session()->get('success') }}</strong> 
  </div>
       @endif
   
			
        </div>			
  <div class="modal-body">
<div class="row bg-light">
				<div class="col">
					<form id="register_form" action="/register" method="POST">
						{{ csrf_field() }}
						<div class="form-row">
						  	<!--div class="form-group col-6">
						  	
						  		<label for="inputDate"> Date of Birth</label>
						  		<input type="date" class="form-control" id="inputDate" placeholder="Data gg/mm/aaaa" name="birth_date">
						  	</div-->
						  	<div class="form-group col-6">
						  	
						  		<label for="inputName"> Name</label>
						  		<input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
						  		@if ($errors->has('name'))
                                  <div class="error" style="color:red;">{{ $errors->first('name') }}</div>
                                @endif
						  	</div>
						  	<!--div class="form-group col-6">
						  		<label for="inputTime"> Timetables</label>
						  		<input type="time" class="form-control" id="inputTime" placeholder="Timetables">
						  	</div-->
						  	<div class="form-group col-6">
						  		<label for="inputEmail"> Email</label>
						  		<input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email">
						  		@if ($errors->has('email'))
                                  <div class="error" style="color:red;">{{ $errors->first('email') }}</div>
                                @endif
						  	</div>
						  
  							<div class="form-group col-6">
  								<label for="password"> Password</label>
								<input type="password" class="form-control" id="inputCel" placeholder="Password" name="password">
								@if ($errors->has('password'))
                                  <div class="error" style="color:red;">{{ $errors->first('password') }}</div>
                                @endif
							</div>
					 	</div>
					

					  	  <div class="modal-footer">
					          <button type="submit" class="btn btn-secondary ">Register</button>
					          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					</form>
				</div>
			</div>
</div>

</div></div></div>



<div class="modal fade" id="mylogin" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        	
	     		 	
				   <h2 class="modal-title">~ Login ~</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
					@if(session()->has('success1'))
   <div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> {{ session()->get('success1') }}</strong> 
  </div>
  @endif
       
   
@if(session()->has('error'))
   <div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> {{ session()->get('error') }}</strong> 
  </div>
  @endif
			<br>
        </div>			
  <div class="modal-body">
<div class="row bg-light">
				<div class="col">
					<form id="login_form" action="/login" method="POST">
						{{ csrf_field() }}
						<div class="form-row">
						  
						  	<!--div class="form-group col-6">
						  		<label for="inputEmail"> Email</label>
						  		<input type="email" class="form-control" id="inputEmail" placeholder="Email">
						  	</div-->
						  	<!--div class="form-group col-6">
						  		<label for="inputNumber"> Number of Guests</label>
						  		<input type="number" class="form-control" id="inputNumber" placeholder="Number of Guests">
						  	</div-->
						  	<div class="form-group col-6">
						  		<label for="inputCel"> Email</label>
						  		<input type="tel" class="form-control" id="inputemail" placeholder="email" name="lemail">
						  		@if ($errors->has('lemail'))
                                  <div class="error" style="color:red;">{{ $errors->first('lemail') }}</div>
                                @endif
						  	</div>
  							<div class="form-group col-6">
  								<label for="password"> Password</label>
								<input type="password" class="form-control" id="inputCel" placeholder="Password" name="lpassword">
								@if ($errors->has('lpassword'))
                                  <div class="error" style="color:red;">{{ $errors->first('lpassword') }}</div>
                                @endif
							</div>
					 	</div>
				

					  	  <div class="modal-footer">
					         	<button type="submit" class="btn btn-secondary ">Login</button>
					          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					</form>
				</div>
			</div>
</div>

</div></div></div>







			<div class="row footer bg-light">
				<div class="col">
					<p class="text-center">Follow us: <a class="social-icon" href="https://www.facebook.com/technologiesShpk/"><i class="fab fa-facebook"></i></a> <a class="social-icon" href="https://www.instagram.com/technologies/"><i class="fab fa-instagram"></i></a></p>
				</div>
				<div class="col">
					<p class="text-center">Copyright &copy; 2018</p>
				</div>
				<div class="col">
					<p class="text-center">Powered by: <a href="https://technologies.com/"> Technologies</a></p>
				</div>
			</div>

		</div>
		
		<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
		<script type="text/javascript" src="{{ asset('js/map.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/smooth-scroll.js') }}"></script>
		<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
		<script type="text/javascript" src="{{ asset('js/image-effect.js') }}"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFZjOV0KA68G2YAh-rn7I3qKqCQEh_Ja0&callback=myMap">
	    </script>
	    <script type="text/javascript">
	$(document).ready(function(){
		if (window.location.href.indexOf("#Register") > -1) 
		{
           $('#myregister').modal('show');
        }
        if (window.location.href.indexOf("#Login") > -1) 
		{
           $('#mylogin').modal('show');
        }
		
	});
</script>
  	</body>
</html>
