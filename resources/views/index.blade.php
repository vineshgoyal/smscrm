@extends('layout')
@section('title','Home Page')

@section('content')
			<!--- Start Carousel -->
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
				    	<img class="d-block w-100 img-fluid img-slider" src="images/banner_1.jpg" alt="First slide">
				    	<div class="carousel-caption">
						    <h2>Welcome!</h2>
							<p>...</p>
						</div>
				    </div>
				    <div class="carousel-item">
				      	<img class="d-block w-100 img-fluid img-slider" src="images/services_banner.jpg" alt="Second slide">
				      	<div class="carousel-caption">
						    <h2>Instant and best quality SMS delivery</h2>
							<p>...</p>
						</div>
				    </div>
				    <div class="carousel-item">
				      	<img class="d-block w-100 img-fluid img-slider" src="images/banner_1.jpg" alt="Third slide">
				      	<div class="carousel-caption">
						    <h2>Better Deliver</h2>
							<p>...</p>
						</div>
				    </div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<br>
               
		<div class="row">
		
			<div class="col-md-4 col-md-offset-4 text-center"></div>
			
					  		<div class="col-md-4 col-md-offset-4 text-center">
                             @if(Auth::check())
                           
	                        	<button type="submit" data-toggle="modal" data-target="#myModal" class="btn btn-secondary btn-block">Start Sending Messages</button>
                             <!-- Modal -->
  
</div>



                             @else
                            	<button type="submit" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#mylogin">Start Sending Messages</button>
                             @endif
					  		
					  		</div>
					  	</div>
			<!--- End of Carousel -->
			<!--- Restaurant-->
		<div class="container">
			
		 	<div class="row" id="About">
	    		<div class="col navMenu">
	     		 	<h2 class="text-center" >~ Why Us ~</h2>
	    		</div>
  			</div>
  			<div class="row bg-light" >
  				<div class="col-md-6">
  					<h3>Location</h3>
  					<p>We help over 165,000 businesses with their SMS-based marketing campaigns, customer service alerts, and updates. We know a thing or two about mobile messaging.</p>
  					<h5>A Unique Experience</h5>
  					<p>The once humble mobile phone has evolved to become the most used communication tool in existence. Over the last nine years, Textlocal has been at the forefront of business mobile messaging. Our in-house, award winning technical team like nothing better than to innovate and build tools optimized for delivery on mobile phones that meet real business needs. We deal with businesses every day. We know the challenges you face and we understand your needs.</p>
  				</div>
  				<div class="col-md-6" data-aos="fade-up">
  					<img class="img-fluid" src="images/lo.jpeg">
  				</div>
  			</div>
  			<div class="row bg-light"><br></div>
  			<div class="row bg-light">
  				<div class="col-md-6 order-md-1 order-2" data-aos="fade-up">
  					<img class="img-fluid " src="images/cu.jpeg">
  				</div>
  				<div class="col-md-6 order-md-12 order-1">
  					<h3>Experience</h3>
  					<p>Our emphasis is on efficiency, integration and ease of use. Our Messenger platform has been built with this in mind, along with some really useful added extras such as tracking, surveys, attachments, ticketing, analytics, campaign management tools and much more.</p>
  					<h5>A Unique Experience</h5>
  					<p>Our ever growing staff base is made up of passionate, dedicated people who believe completely in how Textlocal can revolutionize the communication structure of any business. As the awards keep coming in and our customers remain extremely satisfied, we know Textlocal is an exciting company to be involved with on any level.</p>
  				</div>
  			</div>
  			<!--- End of Restaurant -->
  			<!--- Start of Menu-->
			<div class="row" id="Packages">
				<div class="col navMenu">
	     		 	<h2 class="text-center" >~ Packages ~</h2>
				</div>
			</div>
			<div class="row bg-light">
            @foreach( $plans as $plan )
								<div class="col-md-4" data-aos="slide-up">
									<div class="card view zoom">
											<img class="card-img-top img-fluid " src="images/cu.jpeg">
											<div class="card-body">
												<h5 class="card-title">~ {{ $plan->title }} ~</h5>
												<ul class="list-group list-group-flush">
													<li class="list-group-item">up to {{ $plan->messages_limit }} messages per day.</li>
													<li class="list-group-item">No Custom Sender. Sender will always be ( XX- WAYSMS)</li>
												<li class="list-group-item">Limited group SMSing</li>
												<li class="list-group-item"> Plan will be valid for {{ $plan->validity_days }} days </li>
												<li class="list-group-item">No Lists</li>
											</ul>
												<div class="text-center" >
														<a href="/package/{{$plan->id}}" class="btn btn-success" > Buy </a>
												</div>
											</div>
									</div>
																						
								</div>
          		@endforeach
		</div>
@stop