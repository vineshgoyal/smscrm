<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin | Register</title>

    <link href="{{URL::to('/')}}/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/animate.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            <h3>Register to IN+</h3>
            <p>Create account to see it in action.</p>
            <form class="m-t" role="form" action="{{url('adminpanel/adminregister')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name"  name="name" required="">
                </div>
                
           
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                </div>
               
                <div class="form-group">
                        <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{url('login')}}">Login</a>
            </form>
            <p class="m-t">  </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{URL::to('/')}}/admin/js/jquery-3.1.1.min.js"></script>
    <script src="{{URL::to('/')}}/admin/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="{{URL::to('/')}}/admin/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
    

</body>

</html>
