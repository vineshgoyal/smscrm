@extends('admin.layout.app')
@section('title','Phone')



@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Phone</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                            <strong>Phone</strong>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Add Phone Number</a></li>
                               
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <fieldset class="form-horizontal">
                                            <form class="m-t" role="form" action="/adminpanel/phone/{{$res->id}}" method="post" enctype="multipart/form-data">
                                              {{ method_field('PUT') }}

                                              {{csrf_field()}}
                                            <div class="form-group"><label class="col-sm-2 control-label">Phone:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="enter phone number" name="phone_number" value="{{$res->phone_number}}"></div>
                                            </div>
                                              <div class="form-group"><label class="col-sm-2 control-label">Assigned to:</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="user_id">
                                                        <option value="">CHOOSE USER</option>
                                                        @foreach($result as $res1)
                                                        <option value="{{$res1->id}}" {{ ($res1->id==$res->user_id) ? ' selected="selected"': '' }} >{{$res1->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div> 
                                             <div class="form-group"><label class="col-sm-2 control-label">Assigned from:</label>
                                                <div class="col-sm-10"><input type="date" class="form-control"  name="assigned_from" value="{{$res->assigned_from}}"></div>
                                            </div> 
                                             <div class="form-group"><label class="col-sm-2 control-label">Assigned till:</label>
                                                <div class="col-sm-10"><input type="date" class="form-control"  name="assigned_till" value="{{$res->assigned_till}}"></div>
                                            </div> 
                                            @if($res->status==1)
                                            <div class="form-group"><label class="col-sm-2 control-label">Status:</label>
                                            <div class="col-sm-2"><input type="checkbox" class="form-control"  name="status" id="myCheck" value="1" checked></div>
                                            </div> 
                                             @else
                                              <div class="form-group"><label class="col-sm-2 control-label">Status:</label>
                                            <div class="col-sm-2"><input type="checkbox" class="form-control"  name="status" id="myCheck" value="0" ></div>
                                            </div> @endif
                                            @if(count($errors))
                                        <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @foreach($errors->all() as $error) 
                                                <div>{{$error}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                            <div class="row">
                            <div class="col-sm-4">
                                   <button class="btn btn-primary" type="submit">                                Save</button>
                                        
                            </div>
                        </div>
                                                </div>
                                            </div>
                                            
                                          </form>
                                        </fieldset>

                                    </div>
                                </div>
                               
                                
                            
                            
                    </div>
                </div>
            <script>


    $('#myCheck').on('change', function(){
   this.value = this.checked ? 1 : 0;
   // alert(this.value);
}).change();
</script>
            <script>
    $('.summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
});
</script>
@stop