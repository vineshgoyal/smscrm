@extends('admin.layout.app')
@section('title',' Users Details')


@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Users</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                          <a href="/adminpanel/todo">Users</a>
                        </li>
                        <li class="active">
                          
                            <strong>{{$result->name}} Details</strong></a>
                        </li>
                         
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                       <a href="/adminpanel/user" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

                    </div>
                </div>
            </div>
@stop


@section('content')

<div class="row">
<div class="col-lg-12">

                    <div class="ibox product-detail">
                        <div class="ibox-content">

                            <div class="row">
                                <!--div class="col-md-5">


                                    <div class="product-images slick-initialized slick-slider" role="toolbar">
                                        <img src="{{$result->image}}/341" alt="image" width="100%"   >
                                       </div>

                                </div-->
                                <div class="col-md-7">

                                    <h2 class="font-bold m-b-xs">
                                        {{$result->name}}
                                    </h2>
                                    <small></small>
                                    <div class="m-t-md">
                                        <h3 class="product-main-price">Email: <small class="text-muted">{{$result->email}}</small> </h3>
                                    </div>
                                    <hr>

                                  
                                       <h4>Creation Date:</h4>
                                      {{$date}}

                                    <hr>
                                            <form class="form-horizontal-{{$result->id}}" action="{{'/adminpanel/user/'.$result->id}}"  method="post">
                                          {{csrf_field()}}
                                          {{method_field('PUT')}}

                                      <input type="hidden" name="item_id" value="{{$result->id}}">
                                        @if($result->deleted_at == null)

                                        <button class="btn btn-danger btn-sm btn-delete" type="button" item_id="{{$result->id}}"><strong><i class="fa fa-ban"></i> Block</strong></button>

                                        
                                        @else
                                         <button class="btn btn-danger btn-sm btn-unblock" type="button" item_id="{{$result->id}}"><strong><i class="fa fa-ban"></i> UnBlock</strong></button>
                                         
                                         @endif

                                        </form>
                                    <div>
                                         <div class="btn-group">
                                              <a  href="{{'/adminpanel/message/'.$result->id}}" class="btn btn-sm btn-default"><i class="fa fa-envelope"></i> User Messages </a>
                                             <a  href="{{'/adminpanel/user/'.$result->id.'/edit'}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a> 
                                        </div> 
                                    </div>



                                </div>
                            </div>

                        </div>
                        
                    </div>

                </div>
</div>
 <script>
    $('.btn-delete').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Block it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-'+item_id).submit();
            swal("Blocked!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
<script>
    $('.btn-unblock').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, UnBlock it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-'+item_id).submit();
            swal("Unblocked!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
 @stop