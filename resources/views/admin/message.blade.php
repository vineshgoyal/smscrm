@extends('admin.layout.app')
@section('title','Messages')


@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Messages</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                            <strong>Messages</strong>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </div>
                </div>
            </div>
@stop


@section('content')


 <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <button class="pull-right btn btn-primary" >Send Message</button>
                            <h5>Messages</h5>

                           
                        </div>
                        <div class="ibox-content table-responsive">

                            <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
                                <thead>
                                <tr>

                                   <th class="footable-visible footable-sortable">Sr. No.<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">User_id<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Send To<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Message<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Sent At<span class="footable-sort-indicator"></span></th>
                                   
                                   <!--  <th class="footable-visible footable-sortable">Image<span class="footable-sort-indicator"></span></th>
                                    -->
                                    <th class="footable-visible footable-last-column footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <?php  $count = ($result->perPage() * ($result->currentPage()-1))+1; ?>
                                
                                @foreach($result as $res)
                                
                                <tr style="" class="footable-even">
                                    <td class="footable-visible">{{$count++}}</td>
                                    <td class="footable-visible">{{$res->user->name}}</td>
                                    <td class="footable-visible">{{$res->send_to}}</td>
                                     <td class="footable-visible">{{$res->message}}</td>
                                    <td class="footable-visible">{{date(' j F Y', strtotime($res->created_at))}}</td>
                                   
                                   
                                    <!-- <td class="footable-visible">
                                        <img src="{{'/uploads/'.$res->image}}" alt="image" width="50px" height="50px" style="border-radius:50%;"></td>
                                     -->
                                   
                                    <td class="footable-visible footable-last-column"><!-- <a href="{{'/todo/'.$res->id.'/edit'}}"><button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Edit</strong></button></a> --><!-- <a href="{{'/todo/'.$res->id}}"><button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Details</strong></button></a>&nbsp; -->
                                      

                                      <form class="form-horizontal-{{$res->id}}" action="{{'/adminpanel/message/'.$res->id}}"  method="post">
                                      {{csrf_field()}}
                                      {{method_field('DELETE')}}

                                      <input type="hidden" name="item_id" value="{{$res->id}}">


                                        <button class="btn btn-danger btn-xs btn-delete" type="button" item_id="{{$res->id}}"><strong>Delete</strong></button></form></td>
                                </tr>@endforeach
                               
                            </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="12">
                                    <ul class="pagination pull-right">
                                        {{ $result->links() }}
                                   </ul>
                                   </td>  
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                    <script>
    $('.btn-delete').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-'+item_id).submit();
            swal("Deleted!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
@stop