
@extends('admin.layout.app')
@section('title','AdminProfile')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                       <strong>Admin Profile</strong>
                        </li>
                        
                       
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')


<div class="row">

 @foreach($result as $res)
 <div class="col-lg-4"></div>

<div class="col-lg-4">
                        <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$res->name}}
                            </h2>
                                <small></small>
                            </div>
                            <img src="/admin/img/profile_small.jpg" class="img-circle circle-border m-b-md" alt="profile" width="150px" height="150px">
                           
                        </div>
                        <div class="widget-text-box">
                            <h4 class="media-heading"> {{$res->name}}</h4>
                            <p> {{$res->email}}</p>
                            <div class="text-right">
                                <div class="btn-group">
                                <a  href="{{'/adminpanel/adminprofile/'.$res->id.'/edit'}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a></div>
                               
                        </div>
                </div>
</div>
@endforeach

</div>



  
    
    @stop