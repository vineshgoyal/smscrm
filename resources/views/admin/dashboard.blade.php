
@extends('admin.layout.app')
@section('title','Dashboard')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="">Dashboard</a>
                        </li>
                        
                    </ol>
                </div>
                
            </div>
@stop            

@section('content')


<div class="row">
<div class="col-lg-4">
  <a href="{{url('adminpanel/user')}}"><div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total Users</span>
                                <h5>Users</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$users}}</h1>
                                <div class="stat-percent font-bold text-navy"></div>
                                <small>Total users</small>
                            </div>
                        </div></a>
                    </div> 
<div class="col-lg-4">
      <a href="{{url('adminpanel/message')}}"><div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total Messages</span>
                                <h5>Messages</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$messages}}</h1>
                                <div class="stat-percent font-bold text-navy"></div>
                                <small>Total Messages</small>
                            </div>
                        </div></a>
                    </div> 

<div class="col-lg-4">
      <a href="{{url('adminpanel/phone')}}"><div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total Numbers</span>
                                <h5>Phone Numbers</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">{{$phones}}</h1>
                                <div class="stat-percent font-bold text-navy"></div>
                                <small>Total Numbers</small>
                            </div>
                        </div></a>
                    </div> 






</div>

@stop