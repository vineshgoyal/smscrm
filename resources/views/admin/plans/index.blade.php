@extends('admin.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Plans</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                            <strong>Plans</strong></a>
                        </li>
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        <a href="/adminpanel/plans/create" class="btn btn-primary"><i class="fa fa-plus"></i> Add New Plans</a>
                    </div>
                </div>
            </div>
@stop


@section('content')


<div class="row">
    <div class="col-lg-12">
 
   @foreach($plans as $res)
<div class="col-lg-3">
                <div class="contact-box center-version">

                    <a href="{{'/adminpanel/user/'.$res->id}}">
                        <!--img src="{{$res->image}}/80" class="img-circle" alt="image" width="100%"-->

                        


                        <h3 class="m-b-xs h4"><strong>{{$res->title}} ( {{$res->type}} )</strong></h3>

                        <div class="font-bold clearfix" >${{$res->price}}</div>
                        
                        <div class="font-bold clearfix" >Message Limit <b>{{$res->messages_limit}}</b></div>
                        
                        <div class="font-bold clearfix" >Validity in days <b>{{$res->validity_days}}</b> </div>
                        
                        <br>
                        @if($res->status == 1 )
                        <label class="label label-primary" >Active</label>
                        @else 
                        <label class="label label-danger" >Deactive</label>
                        @endif
                       <!--  <address class="m-t-md">
                            <strong>Address</strong><br>
                            {{$res->address}}<br>
                           
                            <abbr title="Phone">P:</abbr> {{$res->phone_no}}
                        </address>
 -->
                    </a>

                      <div class="contact-box-footer">
                        <div class="m-t-xs btn-group">
                           
                            <a href="{{'/adminpanel/plans/'.$res->id}}" class="btn btn-sm btn-success"><i class="fa fa-info-circle"></i> Edit </a>
                            &nbsp; 
                             <form class=" inline form-horizontal-delete-{{$res->id}}" action="{{'/adminpanel/plans/'.$res->id}}"  method="post">
                                      {{csrf_field()}}
                                      {{method_field('DELETE')}}

                                     

                                        <button class="btn btn-danger btn-sm btn-delete" type="button" item_id="{{$res->id}}"><strong>Delete</strong></button></form>
                           
                        </div>
                    </div>


                </div>
            </div>
          
 @endforeach
</div></div>


                          

     <script>
    $('.btn-delete').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-delete-'+item_id).submit();
            swal("Deleted!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>                
@stop
