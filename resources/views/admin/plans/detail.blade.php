@extends('admin.layout.app')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/user/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                       <a href="/adminpanel/plans"> Plans</a>
                        </li>
                         <li class="active">
                          
                       <strong>Edit Plan Detail</strong>
                        </li>
                        
                       
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/plans" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Plan Detail</a></li>
                               
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <fieldset class="form-horizontal">
                                            <form class="m-t" role="form" action="/adminpanel/plans/{{$plan->id}}" method="post" enctype="multipart/form-data">
                                                {{ method_field('PUT') }}
                                              {{csrf_field()}}
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Title:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="title" value="{{$plan->title}}"></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Type:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="type" value="{{$plan->type}}"></div>
                                            </div>

                                            <div class="form-group"><label class="col-sm-2 control-label"i>Price:</label>
                                                <div class="col-sm-10"><input type="number" class="form-control" placeholder="" name="price" value="{{$plan->price}}"></div>
                                            </div>

                                            <div class="form-group"><label class="col-sm-2 control-label"i>Number of Messages:</label>
                                                <div class="col-sm-10"><input type="number" class="form-control" placeholder="" name="messages_limit" value="{{$plan->messages_limit}}"></div>
                                            </div>

                                             <div class="form-group"><label class="col-sm-2 control-label"i>Validity Days:</label>
                                                <div class="col-sm-10"><input type="number" class="form-control" placeholder="" name="validity_days" value="{{$plan->validity_days}}"></div>
                                            </div>
                                              
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Status:</label>
                                                <div class="col-sm-10"><p></p><input type="checkbox" id="plan_active" placeholder="" {{ $plan->status == 1 ? 'checked' : '' }} value="1" name="status"> <label for="plan_active" >Active</label> </div>
                                            </div>
                                              
                                    @if(count($errors))
                                        <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @foreach($errors->all() as $error) 
                                                <div>{{$error}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                           
                                                    
                                            <div class="row">
                            <div class="col-sm-4">
                                   <button class="btn btn-primary" type="submit"> Update</button>
                                        
                            </div>
                        </div>
                                                </div>
                                            </div>
                                            
                                          </form>
                                        </fieldset>

                                    </div>
                                </div>
                               
                                
                            
                            
                    </div>
                </div>
            
          
@stop