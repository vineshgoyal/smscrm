@extends('admin.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Coupons</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/adminpanel/dashboard">Dashboard</a>
            </li>
            <li class="active">

                <strong>Coupons</strong></a>
            </li>

        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

        </div>
    </div>
</div>
@stop


@section('content')

<div class="ibox float-e-margins">
    <div class="ibox-content table-responsive">
        <a href="{{url('adminpanel/coupons/create')}}" class="btn btn-primary pull-right">Add Coupon</a>
    </div>
</div>

<div class="ibox float-e-margins">

    <div class="ibox-content table-responsive">
        <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
            <tr>
                <th>Title</th><th>Applicable On</th> <th>Discount</th> <th>Experity</th><th>Actions</th>
            </tr>
            @foreach( $coupons as $res)
                <tr>
                    <td><h4>{{$res->title}}</h4></td>
                    <td>
                        @if(isset($res->plan->title))
                        {{$res->plan->title}}
                        @else 
                        All Plan
                        @endif
                    </td>
                    <td>{{$res->discount}}%</td>
                    <td>{{$res->expriry}}</td>
                    <td> 
                        @if( $res->status == 1 )
                       <form action="{{ url('adminpanel/coupons/'.$res->id ) }}" onsubmit="return confirm('Do you realy want to delete this coupon?')" method="post">
                            {{csrf_field()}} {{method_field('DELETE')}}
                            <button class="btn btn-danger" > Make Expire </button>
                        </form>
                        @else
                        <button class="btn text-danger">Expired</button>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@stop
