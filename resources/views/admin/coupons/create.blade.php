@extends('admin.layout.app')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Coupons</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/user/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                       <a href="/adminpanel/coupons"> Coupons</a>
                        </li>
                         <li class="active">
                          
                       <strong>Add Coupon</strong>
                        </li>
                        
                       
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/coupons" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
    <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> New Coupon</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">

                        <fieldset class="form-horizontal">
                            <form class="m-t" role="form" action="{{url('/adminpanel/coupons')}}" method="post" enctype="multipart/form-data">
                              {{csrf_field()}}
                         
                              
                            <div class="form-group"><label class="col-sm-2 control-label"i>Title:</label>
                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="Title" name="title"></div>
                            </div>
                              
                            <div class="form-group"><label class="col-sm-2 control-label"i>Discount:</label>
                                <div class="col-sm-10"><input type="number" class="form-control" placeholder="Discount" name="discount"></div>
                            </div>
                              
                            <div class="form-group"><label class="col-sm-2 control-label"i>Expiry:</label>
                                <div class="col-sm-10"><input type="text" class="form-control input-date" placeholder="Expriry" name="expriry"></div>
                            </div>
                              
                            <div class="form-group">
                                <label class="col-sm-2 control-label"i>Select Plan:</label>
                                <div class="col-sm-10">
                                    <select class="form-control"  name="plan_id" >
                                        <option value="0">All</option>
                                        @foreach( $plans as $plan )
                                        <option value="{{$plan->id}}">{{$plan->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            @if(count($errors))
                                <div class="alert alert-warning alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    @foreach($errors->all() as $error) 
                                        <div>{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-sm-4">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
            
          
@stop