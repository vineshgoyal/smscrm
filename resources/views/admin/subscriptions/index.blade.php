@extends('admin.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Subscriptions</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/adminpanel/dashboard">Dashboard</a>
            </li>
            <li class="active">

                <strong>Subscriptions</strong></a>
            </li>

        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

        </div>
    </div>
</div>
@stop


@section('content')


<div class="ibox float-e-margins">

    <div class="ibox-content table-responsive">
        <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
            <tr>
                <th>Customer</th><th>Mobile</th> <th>Number Of Messages</th> <th>Actions</th>
            </tr>
            @foreach( $plans as $res)
                <tr>
                    <td><h4>{{$res->user->name}}</h4><span class="text-muted" >{{$res->user->email}}</span></td>
                    <td> 
                        {{ $res->twilio_number->phone }}
                        <div  > <label class="label label-info">Expiring on {{ \Carbon\Carbon::parse( $res->created_at )->addDays( $res->validity_days )->format('d/m/Y/H')}}</label></div>
                    </td>
                    <td>
                        {{ $res->messages_limit }}
                    </td>
                    <td>
                        @if( $res->status == 1 )
                            <form action="{{ url('adminpanel/subscribed_plan/'.$res->id ) }}" onsubmit="return confirm('Do you realy want to cancel the plan?')" method="post">
                            {{csrf_field()}} {{method_field('DELETE')}}
                            <button class="btn btn-danger" > Cancel </button>
                        </form>
                        @else 
                        <button type="button" class="btn text-danger bold" > Canceled </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@stop
