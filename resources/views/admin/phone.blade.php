@extends('admin.layout.app')
@section('title','Phone')


@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Phone</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                            <strong>Phone</strong>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        <a href="/adminpanel/phone/create" class="btn btn-primary"><i class="fa fa-plus"></i> Add Phone Number</a>
                    </div>
                </div>
            </div>
@stop


@section('content')


 <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Phone</h5>

                           
                        </div>
                        <div class="ibox-content table-responsive">

                            <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
                                <thead>
                                <tr>

                                   <th class="footable-visible footable-sortable">Sr. No.<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Phone Number<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Assignedto<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Status<span class="footable-sort-indicator"></span></th>
                                  
                                   
                                    <th class="footable-visible footable-last-column footable-sortable">Action<span class="footable-sort-indicator"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <?php  $count = ($result->perPage() * ($result->currentPage()-1))+1; ?>
                                
                                @foreach($result as $res)
                                
                                <tr style="" class="footable-even">
                                    <td class="footable-visible">{{$count++}}</td>
                                    <td class="footable-visible">{{$res->phone_number}}</td>
                                    <td class="footable-visible">{{$res->name}}</td>
                                    <td class="footable-visible">
                                           
                                      <form class="form-horizontal-status-{{$res->id}}" action="{{'/adminpanel/phone/status/'.$res->id}}"  method="post">
                                      {{csrf_field()}}
                                      {{method_field('PUT')}}

                                      <input type="hidden" name="item_id" value="{{$res->id}}">
                                        @if($res->status == 1)

                                        <button class="btn  btn-sm  btn-block1 btn-success"  type="button" item_id="{{$res->id}}"><strong>Activate</strong></button>

                                        
                                        @else
                                         <button class="btn btn-danger btn-sm btn-unblock"  type="button" item_id="{{$res->id}}"><strong><i class="fa fa-ban"></i>Deactivate</strong></button>
                                         
                                         @endif

                                    </form>


                                    </td>
                                  
                                   
                                    <td class="footable-visible footable-last-column">
                                       
                                       <a  href="{{'/adminpanel/phone/'.$res->id.'/edit'}}" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a>
                                      <form class="form-horizontal-delete-{{$res->id}}" action="{{'/adminpanel/phone/'.$res->id}}"  method="post">
                                      {{csrf_field()}}
                                      {{method_field('DELETE')}}

                                      <input type="hidden" name="item_id" value="{{$res->id}}">


                                        <button class="btn btn-danger btn-sm btn-delete" type="button" item_id="{{$res->id}}"><strong>Delete</strong></button></form></td>
                                </tr>@endforeach
                               
                            </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="12">
                                    <ul class="pagination pull-right">
                                        {{ $result->links() }}
                                   </ul>
                                   </td>  
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                    <script>
    $('.btn-delete').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-delete-'+item_id).submit();
            swal("Deleted!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
 <script>
    $('.btn-block1').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, deactivate it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-status-'+item_id).submit();
            swal("Blocked!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
<script>
    $('.btn-unblock').click(function () {
        var item_id = $(this).attr('item_id');        
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Activate it!",
            closeOnConfirm: false
        }, function () {
            $('.form-horizontal-status-'+item_id).submit();
            swal("Unblocked!", "Your imaginary file has been deleted.", "success");        
        });
    });
</script>
@stop