<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SmsCRM|Login</title>

    <link href="{{URL::to('/')}}/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{{URL::to('/')}}/admin/css/animate.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">IN+</h1>

            </div>
            <h3>Welcome to SmsCRM</h3>
            <h4 style="color:red">@if ( count($errors))
  @foreach ($errors->all() as $error)
    {{$error}}
  @endforeach
@endif
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </h2>
            <p></p>
            <form class="m-t" role="form" action="{{url('adminpanel/login')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="email" class="form-control"  name="email" placeholder="Username" >
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" >
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

               <!--  <a href="#"><small>Forgot password?</small></a> -->
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{url('register')}}">Create an account</a> -->
            </form>
            <p class="m-t"> <h4>Admin Login</h4> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{URL::to('/')}}/admin/js/jquery-3.1.1.min.js"></script>
    <script src="{{URL::to('/')}}/admin/js/bootstrap.min.js"></script>




</body>





</html>
