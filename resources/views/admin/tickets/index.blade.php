@extends('admin.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/adminpanel/dashboard">Dashboard</a>
            </li>
            <li class="active">

                <strong>Tickets</strong></a>
            </li>

        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

        </div>
    </div>
</div>
@stop


@section('content')


<div class="ibox float-e-margins">

    <div class="ibox-content table-responsive">
        <table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
            <tr>
                    <th>Customer</th><th>Mobile</th> <th>Actions</th>
            </tr>
            @foreach( $tickets as $res)
                <tr>
                    <td><h4>{{$res->user->name}}</h4><span class="text-muted" >{{$res->user->email}}</span></td>
                    <td>{{$res->phone}} @if( $res->is_closed == 1 ) <div style="margin-top: 5px" ><label class="label label-primary" >Approved</label> </div> @endif </td>
                    <td><a href="/adminpanel/tickets/{{$res->id}}" class="btn btn-info" >View</a> <a class="btn btn-danger" >Delete</a> </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@stop
