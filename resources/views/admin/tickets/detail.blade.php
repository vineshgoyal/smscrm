@extends('admin.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/adminpanel/dashboard">Dashboard</a>
            </li>
            <li class="active">

                <strong>Ticket Detail</strong></a>
            </li>

        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            
            <a href="/adminpanel/tickets" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

        </div>
    </div>
</div>
@stop


@section('content')


<div class="ibox float-e-margins">

    <div class="ibox-content table-responsive">
        <div class='user-detail' >
            <h2 class="bold" >{{ $user->name }} <small class="text-muted" > {{$user->email}} </small> </h2>
            
            <h3><i class="fa fa-phone" aria-hidden="true"></i> {{$ticket->phone}}</h3>
            
            
            @if( isset($ticket->phone) && !empty( $ticket->phone ) && $ticket->is_closed == 0 )
            @if($plan && isset( $plan->price ))
            <div class="form-group" >Plan price ${{ $plan->price }}</div>
            @endif
            @if($coupon && isset( $coupon->title ))
                <label class='label label-success' > <b>{{$coupon->title}}</b> has applied </label>
            @endif
            <form action="{{url('adminpanel/tickets/approve')}}" method="post" onsubmit="return confirm('do you really want to approve this request?')">
                {{csrf_field()}}
                <input type="hidden" name='ticket_id' value="{{$ticket->id}}">
                <h3> Request for <b>{{ $ticket->phone }}</b> <button class="btn btn-primary" >Approve</button></h3>
            </form>
            @else 
            <h4 class="text-success" > Number has approved. </h4>
            @endif
        </div>
    </div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-content table-responsive">
        <div class='user-messages' >
            @foreach( $ticket_messages as $ticket_m )
            
            <div class="{{$ticket_m->admin_reply == 1 ? 'right-message' : 'left-message'}}">
                <span>
                    {{ $ticket_m->message }}
                    
                    <span class="message-time" > {{$ticket_m->created_at->diffForHumans()}} @if( $ticket_m->admin_reply == 1 ) {{$ticket_m->read == 1 ? 'Read' : ''}}  @endif </span>
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>


<div class="ibox float-e-margins">
    <div class="ibox-content table-responsive">
        <form action="/adminpanel/tickets" method="post" >
             {{csrf_field()}}
             <input type="hidden" name="ticket_id" value="{{$ticket->id}}" >
             <textarea placeholder="Type something here" name="message" class="message-text-area" ></textarea>
            <div class="text-right" >
                <button class="btn btn-success" > Send </button>
            </div>
        </form>
    </div>
</div>

@stop
