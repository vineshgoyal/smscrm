@extends('admin.layout.app')
@section('title','Add User')



@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Add User</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/adminpanel/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                            <strong>Add User</strong>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/adminpanel/dashboard" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Add New User</a></li>
                               
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <fieldset class="form-horizontal">
                                            <form class="m-t" role="form" action="/adminpanel/user" method="post" enctype="multipart/form-data">
                                              {{csrf_field()}}
                                            <div class="form-group"><label class="col-sm-2 control-label">Name:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="enter name" name="name" ></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Email:</label>
                                                <div class="col-sm-10"><input type="email" class="form-control" placeholder="enter email" name="email" ></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Password:</label>
                                                <div class="col-sm-10"><input type="password" class="form-control" placeholder="enter password" name="password" ></div>
                                            </div>
                                            
                                             
                                         
                                                  
                                            @if(count($errors))
                                        <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @foreach($errors->all() as $error) 
                                                <div>{{$error}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                            <div class="row">
                            <div class="col-sm-4">
                                   <button class="btn btn-primary" type="submit">                                Save</button>
                                        
                            </div>
                        </div>
                                                </div>
                                            </div>
                                            
                                          </form>
                                        </fieldset>

                                    </div>
                                </div>
                               
                                
                            
                            
                    </div>
                </div>
            <script>
    $('#myCheck').on('change', function(){
   this.value = this.checked ? 1 : 0;
   // alert(this.value);
}).change();
</script>
            <script>
    $('.summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
});
</script>
@stop