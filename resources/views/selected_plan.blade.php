
@extends('layout')
@section('title','Select Plan')

@section('content')


    <!--- Restaurant-->
    <div class="container">
        <br><BR><br>
        <form action="/send_qutation" class="inline" method="post" >
        <div class="row" >
            
            <div class="col-sm-8" >
                @if($ticket_count == -1 )
                <h4 class="text-danger">
                    Please login to select phone number <a class="btn btn-primary" href="" data-toggle="modal" data-target="#mylogin">Login</a>
                </h4>
                @elseif( $ticket_count == -2 )
                <h4 class="text-danger">
                    You already have a plan, you have to first cancel current plan.
                </h4>
                @elseif( $ticket_count > 0 )
                <h3>{{ucwords($plan->title)}}</h3>
                <br>
                <h4 class="text-danger">
                    We have received your Request and it is under process, please wait for admin approval.
                </h4>
                @else
               
                    <h3>{{ucwords($plan->title)}}</h3>
                    <br>
                     {{csrf_field()}}
                    <input type="hidden" name="plan_id" value="{{ $plan->id }}" >
                    <h5>Select Number</h5>
                    <div class="form-group">
                        <select name="country" class="form-control" onchange="getTwilioNumber( this )" >
                   
                            @foreach( $countries as $key => $country)
                                <option value="{{$key}}" @if( Request::query('country') == $key ) selected @endif >{{  $country }}</option>
                            @endforeach
                            <!-- <option>Korea Republic of</option>
                            <option>Latvia</option>
                            <option>Lithuania</option>
                            <option>Luxembourg</option>
                            <option>Macau</option>
                            <option>Malaysia</option>
                            <option>Mali</option>
                            <option>Malta</option>
                            <option>Mauritius</option>
                            <option>Mexico</option>
                            <option>Namibia</option>
                            <option>Netherlands</option>
                            <option>New Zealand</option>
                            <option>Norway</option>
                            <option>Panama</option>
                            <option>Peru</option>
                            <option>Philippines</option>
                            <option>Poland</option>
                            <option>Portugal</option>
                            <option>Puerto Rico</option>
                            <option>Romania</option>
                            <option>Serbia</option>
                            <option>Singapore</option>
                            <option>Slovakia</option>
                            <option>Slovenia</option>
                            <option>South Africa</option>
                            <option>Spain</option>
                            <option>Sudan</option>
                            <option>Sweden</option>
                            <option>Switzerland</option>
                            <option>Taiwan</option>
                            <option>Tanzania</option>
                            <option>Thailand</option>
                            <option>Trinidad and Tobago</option>
                            <option>Tunisia</option>
                            <option>Uganda</option>
                            <option>United Arab Emirates</option>
                            <option>United Kingdom</option>
                            <option>Venezuela</option>
                            <option>Vietnam</option> -->
                        </select> 
                    </div>
                    
                          @if(count( $twilio_numbers ) ) 
                        <div class="form-group">
                            <select name="twilio_number" >
                                 @foreach( $twilio_numbers as $number ) 
                                 <option>{{$number->phoneNumber}}</option> 
                                 @endforeach 
                            </select>
                        </div>
                     @endif 
                    

                    @if(Auth::check() && Request::query('country') )
                    <button type="submit" class="btn btn-primary" >Book Number </button>
                    @endif
                
                @endif
            </div>
            <div class="col-sm-4">
                @if(count($coupons) )
                <h3>Coupons</h3>
                <hr>
                <div class="form-group">
                    <select name="coupon_id" onchange="calculatPrice(this, {{$plan->price}})" >
                        <option value="" selected="" >Select Coupon</option>
                        @foreach( $coupons as $coupon )
                        <option value="{{$coupon->id}}--{{$coupon->discount}}" >{{$coupon->title}} ({{ $coupon->discount }}% Off) </option>
                        @endforeach
                    </select>
                </div>
                @endif
                @if( $ticket_count == 0)
                <h5 class="regular-price-container" >Price: <span class="text-muted regular-price">${{$plan->price}}</span></h5>
                <h5 class="offered-price-container" >Offer Price: <span class="text-muted offered-price ">${{$plan->price}}</span></h5>
                @endif
            </div>
           
                
        </div>
             </form>
    </div>

  @stop
