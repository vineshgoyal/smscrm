@extends('user.layout.app')
@section('title','EditUserProfile')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/user/dashboard">Dashboard</a>
                        </li>
                        <li class="active">
                          
                       <a href="/user/profile"> User Profile</a>
                        </li>
                         <li class="active">
                          
                       <strong>edit User Profile</strong>
                        </li>
                        
                       
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/user/profile" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> User Profile</a></li>
                               
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <fieldset class="form-horizontal">
                                            <form class="m-t" role="form" action="/user/profile/{{$res->id}}" method="post" enctype="multipart/form-data">
                                                {{ method_field('PUT') }}
                                              {{csrf_field()}}
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Name:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="name" value="{{$res->name}}"></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Email:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="email" value="{{$res->email}}"></div>
                                            </div>
                                            <!--div class="form-group"><label class="col-sm-2 control-label"i>Password:</label>
                                                <div class="col-sm-10"><input type="password" class="form-control" placeholder="" name="password" ></div>
                                            </div-->
                                @if(count($errors))
                                        <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @foreach($errors->all() as $error) 
                                                <div>{{$error}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                           
                                                    
                                            <div class="row">
                            <div class="col-sm-4">
                                   <button class="btn btn-primary" type="submit">                                Save</button>
                                        
                            </div>
                        </div>
                                                </div>
                                            </div>
                                            
                                          </form>
                                        </fieldset>

                                    </div>
                                </div>
                               
                                
                            
                            
                    </div>
                </div>
            
            <script>
    $('.summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
});
</script>
@stop