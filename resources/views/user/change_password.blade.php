@extends('user.layout.app')
@section('title','Change Password')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/user/dashboard">Dashboard</a>
                        </li>
                      
                         <li class="active">
                          
                       <strong> Change Password</strong>
                        </li>
                        
                       
                         
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                       <a href="/user/profile" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
@stop
@section('content')

<div class="row">
<div class="col-lg-12">
                    <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Change Password : {{$data->name}}</a></li>
                               
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <fieldset class="form-horizontal">
                                            <form class="m-t" role="form" action="/user/profile/password/{{$data->id}}" method="post" enctype="multipart/form-data">
                                                {{ method_field('PUT') }}
                                              {{csrf_field()}}
                                            <div class="form-group"><label class="col-sm-2 control-label"i>New Password:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="password" ></div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label"i>Confirm Password:</label>
                                                <div class="col-sm-10"><input type="text" class="form-control" placeholder="" name="password_confirmation" ></div>
                                            </div>
                                            <!--div class="form-group"><label class="col-sm-2 control-label"i>Password:</label>
                                                <div class="col-sm-10"><input type="password" class="form-control" placeholder="" name="password" ></div>
                                            </div-->
                                @if(count($errors))
                                        <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @foreach($errors->all() as $error) 
                                                <div>{{$error}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                           
                                      @if (Session::has('message'))
                                       <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <div >{{ Session::get('message') }}</div></div>
                                            @elseif (Session::has('errormessage'))
                                               <div class="alert alert-warning alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <div >{{ Session::get('errormessage') }}</div></div>
                                            @endif              
                                            <div class="row">
                            <div class="col-sm-4">
                                   <button class="btn btn-primary" type="submit">                                Change Password</button>
                                        
                            </div>
                        </div>
                                                </div>
                                            </div>
                                            
                                          </form>
                                        </fieldset>

                                    </div>
                                </div>
                               
                                
                            
                            
                    </div>
                </div>
            
            <script>
    $('.summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
});
</script>
@stop