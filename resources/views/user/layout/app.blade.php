<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SmsCRM|@yield('title')</title>
   
    <link href="{{URL::to('/')}}/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{{URL::to('/')}}/admin/css/animate.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/style.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/admin/css/plugins/summernote/summernote.css" rel="stylesheet" type="text/css">
    <link href="{{URL::to('/')}}/admin/css/plugins/summernote/summernote-bs3.css" rel="stylesheet" type="text/css">

    <!-- Sweet Alert -->
    <link href="{{URL::to('/')}}/admin/sweetalert-master/dist/sweetalert.css" rel="stylesheet" type="text/css">
    <link  href="{{URL::to('/')}}/admin/css/plugins/cropper/cropper.min.css" rel="stylesheet">
     <link rel="stylesheet" href="{{URL::to('/')}}/web/js/source/jquery.fancybox.css">

    <!-- <link href="{{URL::to('/')}}/admin/bootstrap-filestyle/test/css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->


    <!-- date picker -->
    <!--    <link href="{{URL::to('/')}}/admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
 -->

    <script src="{{URL::to('/')}}/admin/js/jquery-3.1.1.min.js"></script>
    <script src="{{URL::to('/')}}/admin/js/bootstrap.min.js"></script>
    <script src="{{URL::to('/')}}/admin/sweetalert-master/dist/sweetalert.min.js"></script>
    <script src="{{URL::to('/')}}/admin/js/plugins/summernote/summernote.min.js"></script>
    <!-- <script src="{{URL::to('/')}}/admin/bootstrap-filestyle/src/bootstrap-filestyle.min.js"> </script> -->
    <!--image cropper-->
    <script src="{{URL::to('/')}}/admin/js/plugins/cropper/cropper.min.js"></script>
     <script src="{{URL::to('/')}}/web/js/source/jquery.fancybox.js"></script>
    
</head>

<body class="">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{URL::to('/')}}/admin/img/profile_small.jpg" />

                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"></strong>
                             </span> <span class="text-muted text-xs block">User&nbsp;{{Auth::User()->name}}<b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{url('/user/profile/'.Auth::User()->id)}}">Profile</a></li>
                            <li><a href="{{url('/user/profile/password/'.Auth::User()->id)}}">Change Password</a></li>
                            <!-- <li><a href="#">Contacts</a></li>
                            <li><a href="#">Mailbox</a></li> -->
                            <li class="divider"></li>
                            <li><a href="{{url('/logout')}}">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        PA
                    </div>
                </li>
                <li>
                    <a href="{{url('/user/dashboard/'.Auth::User()->id)}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> 
                    
                </li>
                <li>
                    <a href="{{url('/home')}}"><i class="fa fa-home" aria-hidden="true"></i> <span class="nav-label">Home</span></a>
                </li>
                @if($canSendMessage)
                <li>
                    <a href="{{url('user/message')}}"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-label">Messages</span></a>
                </li>
                @endif
                <li>
                    <a href="{{url('/user/tickets')}}"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-label">Manage Tickets</span></a>
                </li>
                <li>
                    <a href="{{url('/user/subscribed_plan')}}"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="nav-label">Manage Subscriptions</span></a>
                </li>
                <!--
                <li>
                    <a href="{{url('adminpanel/testimonial')}}"><i class="fa fa-comments"></i> <span class="nav-label">Testimonials</span></a>
                </li>
                <li>
                    <a href="{{url('adminpanel/video')}}"><i class="fa fa-youtube-play"></i> <span class="nav-label">Videos</span></a>
                </li>
                <li>
                    <a href="{{url('adminpanel/cover-images')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Cover Images</span></a>
                </li>
                 <li>
                    <a href="{{url('adminpanel/spotlights')}}"><i class="fa fa-star"></i> <span class="nav-label">Spotlights</span></a>
                </li-->
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!-- <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> -->
        </div>
            <ul class="nav navbar-top-links navbar-right">
               <!--  <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to INSPINIA+ Admin Theme.</span>
                </li> -->
                <li class="dropdown">
                   
                    <ul class="dropdown-menu dropdown-messages">
                       
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    
                   
                </li>


                <li>
                    <a href="{{url('/logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
        @yield('breadcrumbs')
            

            <div class="wrapper wrapper-content">
                @if(Session::has('error'))
                    <div class="alert alert-sm alert-danger" >
                         {{ Session::get('error') }}
                         @php
                         Session::forget('error');
                         @endphp
                    </div>
                @endif
                
                @yield('content')
            </div>
            <div class="footer">
                <div class="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2017
                </div>
            </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    
    <script src="{{URL::to('/')}}/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="{{URL::to('/')}}/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{URL::to('/')}}/admin/js/inspinia.js"></script>
    <script src="{{URL::to('/')}}/admin/js/plugins/pace/pace.min.js"></script>
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEIk0QlBoWXCB1U78J5JX9jUsuEgjIxv4&libraries=places"></script>
<!--DatePicker-->
  <script src="{{URL::to('/')}}/admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>

        <script type="text/javascript">
            function init() {
                var options = {
                    types: ['(regions)']
                };

                var input = document.getElementById('location');
                var autocomplete = new google.maps.places.Autocomplete(input, options);
            }
            google.maps.event.addDomListener(window, 'load', init);
        </script>


<script>
$(document).ready(function(){
$('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format:'yyyy-mm-dd'
            });
});
</script>



</body>

</html>
