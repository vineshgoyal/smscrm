@extends('user.layout.app')
@section('title','Users')

@section('breadcrumbs')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/user/dashboard">Dashboard</a>
            </li>
            <li class="active">

                <strong>Ticket Detail</strong></a>
            </li>

        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="/user/tickets" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>

        </div>
    </div>
</div>
@stop


@section('content')



<div class="ibox float-e-margins">
    <div class="ibox-content table-responsive">
        <div class='user-messages' >
            @foreach( $ticket_messages as $ticket_m )
            
            <div class="{{$ticket_m->admin_reply == 0 ? 'right-message' : 'left-message'}}">
                <span>
                    {{ $ticket_m->message }}
                    
                    <span class="message-time" > {{$ticket_m->created_at->diffForHumans()}} @if( $ticket_m->admin_reply == 1 ) {{$ticket_m->read == 1 ? 'Read' : ''}}  @endif </span>
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div>


<div class="ibox float-e-margins">
    <div class="ibox-content table-responsive">
        <form action="/user/tickets" method="post" >
             {{csrf_field()}}
             <input type="hidden" name="ticket_id" value="{{$ticket->id}}" >
             <textarea placeholder="Type something here" name="message" class="message-text-area" ></textarea>
            <div class="text-right" >
                <button class="btn btn-success" > Send </button>
            </div>
        </form>
    </div>
</div>

@stop
