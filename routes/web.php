<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth', 'prefix' => 'user'], function () {  
    Route::get('tickets','TicketController@index');
    $this->resource('subscribed_plan', 'UserSubscriedPlans');
    Route::get('tickets/{id}','TicketController@messages');
    Route::post('tickets','TicketController@store');
    $this->get('dashboard/{id}','UserController@dashboard');
    $this->get('message','MessageController@userMessage');
    $this->post('message/send','MessageController@sendMessage');

    $this->get('profile/{id?}','UserController@profile'); 
    $this->resource('profile','UserController'); 
    $this->any('profile/password/{id?}','UserController@changePassword');

});

Route::get('/','UserController@index');
Route::get('/package/{plan_id}','UserController@packageDetail');
Route::post('/send_qutation','UserController@sendQutation');



Route::resource('/home','UserController');
Route::any('/register','UserController@register')->name('register');
Route::any('/login','UserController@login')->name('login');
Route::any('/user/{id}','UserController@verification');
Route::any('/sms','UserController@smsSend');
Route::get('/logout', 'UserController@doLogout');

// admin routes
Route::group(['prefix' => 'adminpanel'], function () {  

    $this->get('login', 'Admin\LoginController@login')->name('adminlogin');
    $this->get('register', 'Admin\RegisterController@register');
    $this->post('adminregister', 'Admin\RegisterController@store');
    $this->post('login', 'Admin\LoginController@dologin');
});


Route::group(['middleware' => 'admin', 'prefix' => 'adminpanel'], function () {  
    $this->get('logout','Admin\LoginController@logout');
     
    $this->get('dashboard','Admin\AdminController@dashboard');
    $this->resource('adminprofile','Admin\AdminProfileController'); 
    $this->resource('user','Admin\todoController'); //usercontroller
    $this->resource('message','Admin\MessageController');
    $this->resource('phone','Admin\PhoneController');
    $this->any('phone/status/{id}','Admin\PhoneController@updateStatus');
    $this->any('user/update/{id}','Admin\todoController@updateUser');
    $this->get('message/{id?}','Admin\MessageController@userMessage');
    $this->resource('plans', 'Admin\PlanController');
    $this->resource('subscribed_plan', 'Admin\UserSubscriedPlans');
    $this->post('tickets/approve', 'Admin\TicketController@approve');
    $this->resource('tickets', 'Admin\TicketController');
    $this->resource('coupons', 'Admin\CouponController');
        

});
