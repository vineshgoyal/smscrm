<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'user_id', "twilio_number_id", 'send_to' , 'message', 'account_sid', 'sid' ,'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    protected $table = 'sent_messages';
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
