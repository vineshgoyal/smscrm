<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use App\Plan;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $canSendMessage = false;
            
            if( Auth::check() ) {
                $userPlan = \App\UserPlan::where('user_id', Auth::user()->id)->where('status', 1 )->first();
                if($userPlan){
                    $canSendMessage = true;
                }
//                
//                $startDate = Carbon::parse($userPlan->created_at);
//                $now = Carbon::now();
//                
//                $validity = $now->diffInDays($startDate);
//                
//                $plan = Plan::find( $userPlan->plan_id );
//                if( $plan ) {
//                    if( ($userPlan->validity_days - $validity ) > 0 ){
//                        $canSendMessage = true;
//                    } else {
//                        session(['error' => 'Your Plan "' . $plan->title . '" has expired.']);
//                    }
//                }
                
            }
            $view->with('canSendMessage', $canSendMessage );    
        });  
    
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
