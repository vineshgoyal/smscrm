<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwilioNumber extends Model {
    protected $fillable = [
        'accountSid', "sid", 'phone' , 'capabilities' ,'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    protected $table = 'twilio_numbers';
}
