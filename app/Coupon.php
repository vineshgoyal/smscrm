<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'plan_id', "title", 'status','discount' , 'expriry', 'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    protected $table = 'coupons';
    
    public function plan() {
        return $this->belongsTo('App\Plan');
    }
}
