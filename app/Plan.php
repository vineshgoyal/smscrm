<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
     protected $fillable = [
        'title', 'type', 'price', 'coupon_id' ,'messages_limit', 'validity_days' , 'status' ,'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    
    public function coupons()
    {
        return $this->hasMany('App\Coupons');
    }
}
