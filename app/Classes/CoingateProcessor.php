<?php
namespace App\Classes;
use CoinGate\CoinGate;

class CoingateProcessor
{
    function __construct(){
        \CoinGate\CoinGate::config(array(
            'environment'               => 'sandbox', // sandbox OR live
            'auth_token'                => 'G6WUEqz8h5oEfUvbz36-njaBTt7oVGJNKC4GH7Fz',
            'curlopt_ssl_verifypeer'    => FALSE // default is false
        ));
    }    

    public function createOrder($userPlan)
    {
        $post_params = array(
            'order_id'          => $userPlan->id,
            'price_amount'      => $userPlan->price,
            'price_currency'    => 'USD',
            'receive_currency'  => 'BTC',
            'callback_url'      => 'https://cryptomerchant.eu',
            'cancel_url'        => 'https://example.com/cart',
            'success_url'       => 'https://example.com/account/orders',
            'token'             => $userPlan->token,
            'title'             => $userPlan->plan->title,
            'description'       => $userPlan->user->name.'- '.$userPlan->plan->title.' '.$userPlan->plan->type
        );

        $order = \CoinGate\Merchant\Order::create($post_params);
        if ($order) {
            return $order;
        } else {
            return false;
        }
    }

    public function getOrder($id)
    {
        return \CoinGate\Merchant\Order::find($id);
    }
}

?>