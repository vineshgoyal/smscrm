<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
     protected $fillable = [
        'user_id',"user_plan_id", 'phone', 'is_closed','created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    
    public function UserPlan(){
        return $this->belongsTo('App\UserPlan');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function messages()
    {
        return $this->hasMany('App\TicketMessage');
    }
}