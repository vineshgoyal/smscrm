<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\UserPlan;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Plan;

class MessageController extends Controller
{
    
    public function userMessage()
    {
        
        $messages = Message::where('user_id',Auth::user()->id)->orderBy('id','desc')->with('user')->paginate(10);
        
        $userPlan = \App\UserPlan::where('user_id', Auth::user()->id)->first();
        
        
        
        
        if(!$userPlan){
            return redirect("/user/dashboard/" . Auth::user()->id );
        }
                
                
        return view('user/message',compact('messages'));
    }
    public function sendMessage( Request $request ){
        
        $validator = Validator::make($request->all(),[
            'message' => 'required',
            'phone' => 'required'
        ])->validate();
        $userId = Auth::user()->id;
        
        $user_plan = UserPlan::where( 'user_id', $userId )->where( 'status', 1 )->first();
        
        if($user_plan){
            $canSendMessage = true;
        }

        $startDate = Carbon::parse($user_plan->created_at);
        $now = Carbon::now();

        $validity = $now->diffInDays($startDate);

        $plan = Plan::find( $user_plan->plan_id );
        if( $plan ) {
            if( ( $user_plan->validity_days - $validity ) < 1 ){
                return redirect("user/message")->with('error', 'Your Plan "' . $plan->title . '" has expired.' );
            }
        }

        
        $twilio_number_id = $user_plan->twilio_number_id;
        $twilio_number = \App\TwilioNumber::find($twilio_number_id);
        $from_phone = $twilio_number->phone;
        
        $sid = Config::get('twilio.sid');
        $token = Config::get('twilio.token');
        $client = new Client($sid, $token);
        $numbers = $client->messages->create( $request->get('phone') , 
                array("from" => $from_phone, "body" => $request->get('message'))
        );
        
        $message = new Message();
        $message->user_id = $userId;
        $message->twilio_number_id = $twilio_number_id;
        $message->message = $request->get('message');
        $message->send_to = $request->get('phone');
        $message->account_sid = $numbers->accountSid;
        $message->sid = $numbers->sid;
        $message->save();
        
        return redirect("user/message");
    }
}
