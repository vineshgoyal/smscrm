<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use Redirect;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Mail;
use App\Plan;
use Aloha\Twilio\Support\Laravel\Facade as Twilio;
use Twilio\Rest\Client;
use App\Ticket;
use App\Coupon;
use Config;
use App\Services\Payment\Contracts\PaymentContract;
use App\Classes\CoingateProcessor;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {//print_r(1);
        
        $plans =  Plan::where('status', 1)->get();
        // if(Auth::check())
        //     {
        //         $user_sms_details = Message::where('user_id', Auth::User()->id)->orderBy('created_at','desc')->take(5)->get();  
        //         return view('index')->with(compact('user_sms_details'));
        //     }
        // else
        return view('index', ['plans' => $plans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result= User::find($id);
        // dd($result);
      return view('user/edituserprofile',compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res= User::find($id);
        return view('user/edituserprofile',compact('res'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $id;
         
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
       
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->getMessageBag()->first())
                        ->withInput();
        }

         $user = User::find($id);
        
        $user->name= $request->name;
        $user->email= $request->email;
     
        $user->save($request->all());
        //session()->flash('message','update successfully');

        return redirect("user/dashboard/".$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }



    // register user
    public function register(Request $request)
    {
         //Check Validation
        $input = $request->all();
        $validation = Validator::make($input, User::$SignUpRules);
        if($validation->fails())
             return redirect('/home#Register')->withErrors($validation);
            //return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

       $response = User::SignUp($request);
        if ($response)
        {
                   $user     = Input::get('email');
                   $id= $response->id;
           Mail::to('email',array('user' => $user,'id'=>$id), function($message) use ($user,$id)
              {
                 $message->from('12cdeepika@gmail.com', 'SmsCRM');
                 $message->subject('Welcome Mail');
                 $message->to($user);
     
               });


            return redirect('/home#Register')
            ->with(['success' => 'Congratulations! your account is registered, you will shortly receive an email to activate your account.']);
    }

    }


     public static function login(Request $request) {
        
        //Check Validation
        $input = $request->all();
        $validation = Validator::make($input, User::$LoginRules);
        if($validation->fails())
            return redirect('/home#Login')->withErrors($validation);

     $response = User::Login($request);
    // print_r($response);
     if(isset($response))
          {

           

            return redirect('/home');
    }
    }

    public function doLogout()
{
    Auth::logout(); // log the user out of our application
    return Redirect::to('/home'); // redirect the user to the login screen
}

 public function verification($id)
{
    $user_details = User::where('id', $id)->update(['verified' => 1]);
     return Redirect::to('/home#Login')->with(['success1' => 'Congratulations! your account is verified, Please login.']);
}
   public function smsSend()
{

      $phone     = Input::get('phone');
      $message     = Input::get('message');
    //  print_r($phone );
     // print_r( $message);
// $account_id = 'AC69c42c7d839d101131b0c681b8ffe307';
// $auth_token = '3e2810e93a6c51e958fa69a0f5c5e8c2';
// $from_phone_number = '+12012319130'; // phone number you've chosen from Twilio
// $twilio = new Twilio($account_id, $auth_token, $from_phone_number);

// //$to_phone_number = '+37062218617'; // who are you sending to?
// $twilio->message($phone, $message1);


  $sms=Twilio::message($phone, $message);
if($sms)
{
      $user_id = DB::table('messages')->insertGetId(array(
         
            'user_id' => Auth::User()->id,
            'send_to' => $phone ,
            'message' => $message,
         
        
        ));

        $user_sms_details = Message::where('user_id', Auth::User()->id)->get();

        //Send response
      //  return $user_details;
         return Redirect::to('/home#sms')->with(['success2' => 'sms sent successfully.']);
}

}

 public function dashboard($id)
    {
       // $users = User::get()->count();
        $messages = Message::where('user_id',$id)->count();

        return view('user/dashboard', array('messages' => $messages));
    }

    public function profile($id=null)
    {
         $result= User::where('id',$id)->orderBy('id','desc')->first();
        // dd($result);
        return view('user/profile',compact('result'));
    }

        //user password change
        public function changePassword( $id=null)
        {
            if( Auth::id()!=$id ) 
            {
                return Redirect::to('user/dashboard/'.$id);
            } 
            else {
                if(!empty($_POST))
                {
                    $rules = array(
                    'password'                  => 'required|min:6|confirmed',
                    'password_confirmation'     => 'required',
                    );
                    $messages = array(
                    'password.min'        => "Password length should not be less than 6 characters",
                    'password.confirmed'  => "Password does not match",
                    );
                    $validator = Validator::make( Input::all(), $rules, $messages ); 
                    
                    if ($validator->fails()) 
                    {
                        Session::flash('errormessage', 'Password could not be changed, Please correct errors');
                        return Redirect::to('user/profile/password/'.$id)->withErrors($validator);
                    } 
                    else{
                        $user = User::find($id);
                        $user->password     = Hash::make(Input::get('password'));
                        if($user->save()){
                            return Redirect::to('user/profile/password/'.$id)->with('message', 'User password has been changed successfully.');
                        }
                    }
                }
                
                $data = User::find($id);
                 return view('user.change_password', compact('data'));
            }
        }
    function packageDetail( $plan_id ){
        if( !$plan_id ){
            return Redirect::to('/')->with("error", "Please select any one plan");
        }
        $countries = [
            "" => "Please select county",
            "US" =>"United State",
            "DZ" => "Algeria",
            "AR" =>"Argentina",
            "AU" =>"Australia",
            "AT" => "Austria",
            "BB" =>"Barbados",
            "BY" =>"Belarus",
            "BE" =>"Belgium",
            "BJ" =>"Benin",
            "BO" =>"Bolivia",
            "BA" =>"Bosnia and Herzegovina",
            "BW" =>"Botswana",
            "BR" =>"Brazil",
            "BG" =>"Bulgaria",
            "CA" =>"Canada",
            "KY" =>"Cayman Islands",
            "CL" =>"Chile",
            "CO" =>"Colombia",
            "HR" =>"Croatia",
            "CY" =>"Cyprus",
            "CZ" =>"Czech Republic",
            "DK" =>"Denmark",
            "DO" =>"Dominican Republic",
            "EC" =>"Ecuador",
            "SV" =>"El Salvador",
            "EE" =>"Estonia",
            "FI" =>"Finland",
            "FR" =>"France",
            "GE" =>"Georgia",
            "DE" =>"Germany",
            "GH" =>"Ghana",
            "GR" =>"Greece",
            "GD" =>"Grenada",
            "GT" =>"Guatemala",
            "GN" =>"Guinea",
            "HK" =>"Hong Kong",
            "HU" =>"Hungary",
            "IS" =>"Iceland",
            "IN" =>"India",
            "ID" =>"Indonesia",
            "IE" =>"Ireland",
            "IL" =>"Israel",
            "JM" =>"Jamaica",
            "JP" =>"Japan",
            "KE" =>"Kenya"
        ];
        $ticket_count  = -1;
        $plan = [];
        $coupons = [];
        $numbers = [];
        
        
        if( Auth::check() ) {
            $userPlan = \App\UserPlan::where('user_id', Auth::user()->id)->where('status', 1)->count();
//            dd($userPlan);
            if( $userPlan ) {
                $ticket_count = -2; //have already plan
            }else {
                // not have plan already
                $ticket_count = Ticket::where('user_id', Auth::user()->id)->where('is_closed', 0)->count();
        //        dd($ticket_count);
                $numbers = [];
                $plan = Plan::find( $plan_id );
//                dd($plan);
                $coupons = Coupon::whereIn('plan_id', [0,$plan->id])->get();
//                dd($coupons);
                if( !$plan ){
                    return Redirect::to('/');
                }
                $ticket_count =0;
                if(\Request::query('country') ){
                    // Your Account SID and Auth Token from twilio.com/console
                    $sid = 'AC0d0845304d42bb3a0660561c048c8c0b';
                    $token = 'fd1a4299a8800ee6d0b719d1bc241a04';
                    $client = new Client($sid, $token);
                     $numbers = $client->availablePhoneNumbers($_GET['country'])->local->read(
                             //"smsEnabled" => true, 'contains' => "88", "smsEnabled" => false
                         array()
                     );
        //            dd($numbers);
                }
            }
            
        }

       
        return view("selected_plan", [
            'plan' => $plan, 
            'twilio_numbers' => $numbers, 
            'countries' => $countries, 
            'ticket_count'=>$ticket_count ,
            'coupons' => $coupons
        ]);
        
    }
    
    function sendQutation(Request $request){
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'plan_id' => 'required|integer',
            'country' => 'required',
            'twilio_number' => 'required',
            ])->validate();
        
        $coupon_id = $request->get('coupon_id') ? $request->get('coupon_id') : 0;
        
        $plan = Plan::find($request->get('plan_id'));
        
        
        $userPlan = new \App\UserPlan();
        $userPlan->user_id = Auth::user()->id;
        $userPlan->plan_id = $plan->id;
        $userPlan->price = $plan->price;
        $userPlan->coupon_id = $coupon_id;
        $userPlan->messages_limit = $plan->messages_limit;
        $userPlan->validity_days = $plan->validity_days;
        $userPlan->status = 0;
        $userPlan->save();
        
        $ticket = new Ticket();
        $ticket->user_id = Auth::user()->id;
        $ticket->user_plan_id = $userPlan->id;
        $ticket->phone = $request->get('twilio_number');
        $ticket->save();
        
        $ticket_message = new \App\TicketMessage();
        $ticket_message->admin_reply = 0 ;
        $ticket_message->ticket_id = $ticket->id ;
        $ticket_message->message= Auth::user()->email . " is interested in plan " . $plan->title ." . This user has seleted " .  $request->get('twilio_number') . "(". $request->get('country') .") twilio number";
        $ticket_message->save();
        
        // Payment GateWay
        //$CoingateProcessor = new CoingateProcessor();
        //$order = $CoingateProcessor->createOrder($userPlan);
        //if($order){
        //    return redirect()->away($order->payment_url);
        //}
        return redirect("/");
    }

}
