<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPlan;

class PaymentProcess extends Controller
{
    public function callBack()
    {
        $orderData = $_POST;
        if(!empty($orderData)){
            $userOrder = UserPlan::find($orderData['order_id']);
            if($userOrder['token'] == $userOrder->token){
                if($userOrder['status'] == 'paid'){
                    $userOrder->status = 1;
                }
                if($userOrder['status'] == 'invalid'){
                    $userOrder->status = 4;
                }
                if($userOrder['status'] == 'expired'){
                    $userOrder->status = 3;
                }
                if($userOrder['status'] == 'canceled'){
                    $userOrder->status = 2;
                }
            }
            $userOrder->status = 4;
            $user->save();
        }
    }
}
