<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Ticket;
use App\User;
use App\TicketMessage;
use Validator;

class TicketController extends Controller
{

    public function index()
    {
        $tickets = Ticket::select('id','user_id', 'phone','is_closed')->where('user_id', Auth::User()->id)->get();
        
        return view('user.tickets.index', ['tickets' => $tickets]);
        
    }
    
    public function messages( $ticketId ){
        $ticket = Ticket::find($ticketId);
//        dd($ticket->toArray());
        $user = User::find($ticket->user_id);
        $ticket_messages = TicketMessage::where('ticket_id', $ticket->id)->get();
        TicketMessage::where('ticket_id', $ticket->id)->where('admin_reply', 1)->update(['read' => 1]);
//        dd($tickets);
        return view('user.tickets.detail',['ticket' => $ticket, 'user'  => $user, 'ticket_messages' => $ticket_messages ]);
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'message' => 'required',
            'ticket_id' => 'required|integer'
        ])->validate();

        $ticket = new TicketMessage();
        $ticket->ticket_id=$request->input('ticket_id');
        $ticket->admin_reply=0;
        $ticket->message=$request->input('message');
        $ticket->save();
        return redirect("user/tickets/". $request->get('ticket_id'));
    }
    
    
}
