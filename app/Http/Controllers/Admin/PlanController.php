<?php

namespace App\Http\Controllers\Admin;

use App\Plan;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Twilio\Rest\Client;


class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::orderBy('id','desc')->paginate(10);
        return view('admin.plans.index', ['plans' => $plans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'title' => 'required|unique:plans,title|max:255',
            'type' => 'required:255',
            'price' => 'required|integer',
            'messages_limit' => 'required|integer',
            'validity_days' => 'required|integer',
            'status' => 'boolean'
        ])->validate();

        $plan = new Plan();
        $plan->title=$request->input('title');
        $plan->type=$request->input('type');
        $plan->price=$request->input('price');
        $plan->messages_limit=$request->input('messages_limit');
        $plan->validity_days=$request->input('validity_days');
        $plan->status=$request->input('status') ? $request->input('status') : 0;
        $plan->save();
        return redirect("adminpanel/plans");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Plan $plan)
    {
        $plan= Plan::find($plan->id);
//         dd($plan);
      return view('admin.plans.detail',['plan' => $plan ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Plan $plan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plan $plan)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required|unique:plans,title,'.$plan->id.'|max:255',
            'type' => 'required:255',
            'price' => 'required|integer',
            'messages_limit' => 'required|integer',
            'validity_days' => 'required|integer',
            'status' => 'boolean'
        ])->validate();
        
        $plan = Plan::find($plan->id);
        $plan->title=$request->input('title');
        $plan->type=$request->input('type');
        $plan->price=$request->input('price');
        $plan->messages_limit=$request->input('messages_limit');
        $plan->validity_days=$request->input('validity_days');
        $plan->status=$request->input('status') ? $request->input('status') : 0;
        $plan->save();
        return redirect("adminpanel/plans/".$plan->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan)
    {
        $plan->delete();
        return redirect("adminpanel/plans");

    }
}
