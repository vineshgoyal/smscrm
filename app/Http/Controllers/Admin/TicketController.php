<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;
use App\User;
use Validator;
use App\TicketMessage;
use Twilio\Rest\Client;
use App\Plan;
use App\TwilioNumber;
use App\UserPlan;
use Exception;
use Config;
class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::select('id','user_id','phone','user_plan_id','is_closed')->with('UserPlan')->with('user')->get();
        
        return view('admin.tickets.index', ['tickets' => $tickets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'message' => 'required',
            'ticket_id' => 'required|integer'
        ])->validate();

        $ticket = new TicketMessage();
        $ticket->ticket_id=$request->input('ticket_id');
        $ticket->admin_reply=1;
        $ticket->message=$request->input('message');
        $ticket->save();
        return redirect("adminpanel/tickets/". $request->get('ticket_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ticketId)
    {
        $ticket = Ticket::where('id',$ticketId)->with('UserPlan')->first();
        $coupon = null;
        $plan = null;
        if($ticket->UserPlan){
             $coupon = \App\Coupon::where('id', $ticket->UserPlan->coupon_id)->first();
    //        dd($coupon);
            $plan = $ticket->UserPlan->with('plan')->first();
        }
       
//        dd($plan);
        $user = User::find($ticket->user_id);
        $ticket_messages = TicketMessage::where('ticket_id', $ticket->id)->get();
        TicketMessage::where('ticket_id', $ticket->id)->where('admin_reply', 0)->update(['read' => 1]);
//        dd($coupon);
        return view('admin.tickets.detail',['ticket' => $ticket, 'coupon'=>$coupon,'plan' => $plan, 'user'  => $user, 'ticket_messages' => $ticket_messages ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function approve( Request $request ){
        try{
            $validator = Validator::make($request->all(),[
                'ticket_id' => 'required|integer'
            ])->validate();

            $sid = Config::get('twilio.sid');
//            dd($sid);
            $token = Config::get('twilio.token');
            $client = new Client($sid, $token);



            $ticket = Ticket::find($request->input('ticket_id'))->with('UserPlan')->first();
//            dd($ticket->toArray());
            
            $plan = Plan::find($ticket->plan_id);
    //        dd($plan);
            if( isset( $ticket->phone ) && !empty(  $ticket->phone ) ) {
                $number = $client->incomingPhoneNumbers->create(
                    array(
                        "phoneNumber" => $ticket->phone
                    )
                );
                $ticket->is_closed = 1;
                $ticket->save();

                $twilio_number = new TwilioNumber();
                $twilio_number->accountSid = $number->accountSid;
                $twilio_number->sid = $number->sid;
                $twilio_number->phone = $number->phoneNumber;
                $twilio_number->capabilities = json_encode($number->capabilities);
                $twilio_number->save();

                $ticket->UserPlan->twilio_number_id = $twilio_number->id;
                $ticket->UserPlan->status = 1;
                $ticket->UserPlan->save();

            }
            return redirect("adminpanel/tickets/". $request->get('ticket_id'));
        } catch (Exception $ex) {
            return back()->with("error", $ex->getMessage() );
        }
    }
}
