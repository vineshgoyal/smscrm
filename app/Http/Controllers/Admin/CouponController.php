<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coupon;
use App\Plan;
use Validator;
class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('id','desc')->paginate(10);
        return view('admin.coupons.index', ['coupons'=> $coupons ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = Plan::where('status', 1)->get();
        return view('admin.coupons.create', ['plans' => $plans]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required|unique:coupons,title|max:255',
            'discount' => 'required|integer',
            'expriry' => 'required',
            'plan_id' => 'required|integer'
        ])->validate();
        
        $coupon = new Coupon();
        $coupon->title=$request->input('title');
        $coupon->discount=$request->input('discount');
        $coupon->expriry=$request->input('expriry');
        $coupon->plan_id=$request->input('plan_id');
        $coupon->status=$request->input('status') ? $request->input('status') : 1;
        $coupon->save();
        return redirect("adminpanel/coupons");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $coupon =  Coupon::find($id);
            $coupon->status=0;
            $coupon->save();
            return redirect("adminpanel/coupons");
        } catch (Exception $ex) {
            return redirect("adminpanel/coupons")->with('error',$ex->getMessage() );
        }
    }
}
