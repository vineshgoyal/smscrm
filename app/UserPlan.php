<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    protected $fillable = [
        'user_id', "twilio_number_id", 'plan_id', 'coupon_id' , 'price' ,'messages_limit', 'validity_days' ,'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    protected $table = 'user_plan';

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function plan() {
        return $this->belongsTo('App\Plan');
    }
    
    public function coupon() {
        return $this->belongsTo('App\Coupon');
    }
    
    public function twilio_number() {
        return $this->belongsTo('App\TwilioNumber');
    }
    
}
