<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
     protected $fillable = [
        'ticket_id', "admin_reply", 'mesage' , 'read' ,'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    protected $table = 'ticket_messages';
}